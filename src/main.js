// src/main.js

const express = require('express');
const app = express();
const port = 3000;
const mylib = require('./mylib');

console.log(
    {
        sum: mylib.sum(1, 1),
        random: mylib.random(),
        arrayGen: mylib.arrayGen()
    })


app.get('/', (req, res) => {
    res.send('Hello world');
});

app.listen(port, () =>{
    console.log('Server: http://localhost:${port}');
});    