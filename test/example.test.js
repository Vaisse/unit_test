// test/example.test.js


const expect = require('chai').expect;
const should = require('chai').should();
const {assert} = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () =>
{
    let myvar = undefined;

    before(() => 
    {
        myvar = 1;
        console.log('Before testing...');
    })

    it('Should return 2 when using sum function with a=1, b=1', () => 
    {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2); //result expected to equal 2
    });

    it('Parametrized unit testing', () =>
    {
        const result = mylib.sum(myvar, myvar);
        expect(result).to.equal(myvar+myvar);
    })

    it('Assert foo is not bar', () => 
    {
        assert('foo' !== 'bar') //true
    });

    it('Myvar should exist', () =>
    {
        should.exist(myvar);
    })

    after(() => 
    {
        console.log('After testing...');
    })

})